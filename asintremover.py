#!/usr/bin/env python3

"""asintremover.py: It removes associated-interface from "exe fmpolicy" object dump"""

__author__ = "Lukasz Korbasiewicz"
__maintainer__ = "Lukasz Korbasiewicz"
__email__ = "lkorbasiewicz@fortinet.com"
__version__ = "1.1"
__status__ = "Production"

import os
import sys


BLUE ='\033[94m'
GREEN = '\033[92m'
YELLOW = '\033[93m'
RED = '\033[91m'
NORM = '\033[0m'

# Clear the terminal and displaying welcome message
os.system('cls' if os.name == 'nt' else 'clear')
print('\n\n\n\n\nWelcome!\nThis is FortiOS Associated Interface remover v1.0\n')


# Open path
path = "./"
dirs = os.listdir(path)

# Create empty filelist
cfgfiles = []
cfgindex = 0

# This will add all .conf, .txt and .log to list "cfgfiles"
for file in dirs:
    if ".txt" in file:
        cfgfiles.append(file)
        print(str(cfgindex) + " - " + file)
        cfgindex += 1
if cfgindex > 1:
    cfgindex = input("\nWhich file you want to open? Type number only! > ")
    cfgindex = int(cfgindex)
    cfgfile = cfgfiles[cfgindex]
elif cfgindex == 1:
    print('\nOnly one file found in current directory, I will use it')
    cfgfile = cfgfiles[0]
else:
    print('\nNo *.txt, *.conf or *.log files found. Exit!')
    sys.exit()
print('\nReading file: ' + cfgfile + '\n')
f = open(cfgfile)
cfg = f.readlines()
print('\nDone!\n')
f.close()

filename = "./new_" + cfgfile
newfile = open(filename,'w+')

for position,item in enumerate(cfg):
    if item.startswith("set associated-interface"):
        cfg[position] = ""


for line in cfg:
    newfile.write(line)
newfile.close()