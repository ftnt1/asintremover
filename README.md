# README #

It removes associated-interface from "exe fmpolicy" object dump

## Files ##

### asintremover.py ###

This script removes associated-interface from "exe fmpolicy print-adom-object" address dump

Run it from within the directory containing .txt file with the dump. It will create new_dumpfilename.txt

It takes no command line arguments

### README.md ###

This file

## Usage ##

On FortiManager dump some addresses:

```
rz-fortimanager-s # execute fmpolicy print-adom-object 3 140 all
Dump all objects for category [firewall address] in adom [root]:
---------------
edit "aaaaa"
set uuid 3edda4a2-10d9-51e8-2967-0d3d39f7a159
set associated-interface "sslvpn_tun_intf"
set subnet 2.2.2.2 255.255.255.255

next
```

Save it to a .txt file, like "foo.txt"
Run a script
It will create "new_foo.txt"

It will not have "set associated-interface <WHATEVER>" any more

```
rz-fortimanager-s # execute fmpolicy print-adom-object 3 140 all
Dump all objects for category [firewall address] in adom [root]:
---------------
edit "aaaaa"
set uuid 3edda4a2-10d9-51e8-2967-0d3d39f7a159
set subnet 2.2.2.2 255.255.255.255

next
```